# Conda Mojo Docker

Dockerfile to generate a Docker image with [Conda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html) or [Pixi](https://prefix.dev/) and `Mojo` language with `conjo` command to use `Python` packages in `Mojo` code.

Contributions are welcome.

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/yellow_img.png)](https://www.buymeacoffee.com/EchoPouet)

## Build image

Command to build:
```bash
docker build -t conda-mojo .
```

You can change the Linux distribution (only based on Debian) with `IMAGE` argument like this:
```bash
docker build -t conda-mojo --build-arg IMAGE=nvidia/cuda:12.3.1-devel-ubuntu22.04 .
```

## Configure Conda and Mojo

To use the good `Python` library defined by `Conda` in `Mojo`, a command named `conjo` must be called after activate the good environment.

Example with `my_env` that contains `numpy`, first I create an environment and install `Python`:
```bash
(base) root@a3739916ff3f:/# conda create -n my_env
(my_env) root@a3739916ff3f:/# conda activate my_env
(my_env) root@a3739916ff3f:/# conda install -y python==3.10
```

Now I run `conjo` command:
```bash
(my_env) root@a3739916ff3f:/# conjo
Mojo now use Python library path /root/miniconda/envs/my_env/lib/libpython3.so
```

To check if everything's okay, run the following example with `numpy`:
```bash
(my_env) root@a3739916ff3f:/# conda install -y numpy
(my_env) root@a3739916ff3f:/# echo -e 'from python import Python as py 
fn main() raises: 
    var np = py.import_module("numpy") 
    print(np.random.randn(3,7))' > mojo-numpy.mojo
(my_env) root@a3739916ff3f:/# mojo mojo-numpy.mojo
[[ 0.06678725  0.52140626  1.31582905 -0.64345715  2.05215235 -0.10553183
  -1.98780506]
 [ 1.11534337  0.08838308  0.41892181 -0.42804385  0.83975412 -0.12983961
   0.70618928]
 [ 1.10961228 -0.38350084  0.17534197 -0.68237957 -1.19833568  0.60847395
   1.86596285]]
```

If you have this results, your environment works.