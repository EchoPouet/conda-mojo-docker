ARG IMAGE=ubuntu:22.04

FROM ${IMAGE}

# Labels
LABEL org.opencontainers.image.source="https://gitlab.com/EchoPouet/conda-mojo-docker"
LABEL org.opencontainers.image.description="Mojo and Conda Development Container"
LABEL org.opencontainers.image.licenses=MIT

# Variables
ARG CONDA_VER=latest
ARG OS_TYPE=x86_64

# Install packets
RUN apt-get update && apt-get install -yq llvm curl git && rm -rf /var/lib/apt/lists/*

# Set LLVM symbolizer in env
ENV LLVM_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer

# Install Miniconda
RUN curl -LO "http://repo.continuum.io/miniconda/Miniconda3-${CONDA_VER}-Linux-${OS_TYPE}.sh"
RUN bash Miniconda3-${CONDA_VER}-Linux-${OS_TYPE}.sh -p /root/miniconda -b
RUN rm Miniconda3-${CONDA_VER}-Linux-${OS_TYPE}.sh
ENV PATH=/root/miniconda/bin:${PATH}
RUN conda init bash && \
    . /root/.bashrc && \
    conda update -y conda

# Install Modular CLI / Mojo
# https://github.com/modularml/mojo/blob/nightly/CONTRIBUTING.md#getting-the-nightly-mojo-compiler
RUN curl https://get.modular.com | sh -
# https://github.com/MoSafi2/BlazeSeq/blob/main/.github/workflows/package.yml
RUN modular auth examples
RUN modular install mojo
ENV MODULAR_HOME="/root/.modular"
ENV PATH="$MODULAR_HOME/pkg/packages.modular.com_mojo/bin:$PATH"

# Add fonction conjo
RUN echo '# Fonction to update Mojo with the Conda env\n\
conjo() {\n \
    libpython=$(find ${CONDA_PREFIX} -iname "libpython*.so" | sort -r | head -n 1)\n \
    if [ -z "${libpython}" ]\n \
    then\n \
        echo "No Python library found in Conda env !"\n \
    else\n \
        echo "Mojo now use Python library path $libpython"\n \
        export MOJO_PYTHON_LIBRARY=$libpython\n \
    fi\n \
}' >> /root/.bashrc

# Set shell
SHELL ["/bin/bash", "-c"]
